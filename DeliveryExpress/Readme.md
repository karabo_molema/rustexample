#Introduction

This is a simple enterprise application that
I am building to upskill myself in Rust.
 
 The aim of this application is to provide facilitate deliveries for online based orders from various organizations.
 The application will use the following stack
 * Database: PostgreSQL 13
 * Messaging: RabbitMQ
 * ORM: Diesel
 * Web framework: Actix-web
 
 Currently the app starts up and writes one record to database, next steps are to connect this application to a rabbitMQ queue
 pickup the message and save it to database. Following that is to start introducing some architectural patterns
 specifically an adaptation to this project the CQRS pattern.
 
 