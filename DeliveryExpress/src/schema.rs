table! {
    stops (uuid) {
        uuid -> Uuid,
        name -> Varchar,
    }
}
