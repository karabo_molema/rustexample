
use crate::schema::stops;

use uuid::Uuid;


pub enum VehicleType {
    Taxi,
    Minibus,
    Sedan,
    Hatchback,
    Bicycle
}

pub enum OrderType {
    Internal,
    External
}

pub enum OrderStatus {
    Paid,
    UnPaid,
    PartiallyPaid
}

pub enum Currency{
    Rand,
    USDollar,
    BritishPound,
    ZimDollar,
    Pula
}

#[derive(Queryable)]
struct Route {
    name: String,
    stops: Vec<Stop>
}

#[derive(Debug, Clone, Queryable, Insertable)]
#[table_name = "stops"]
pub struct Stop {
    uuid: Uuid,
    name: String
}

#[derive(Queryable)]
pub struct Order {
    order_type: OrderType,
    status: OrderStatus,
    amount: Amount
}

#[derive(Queryable)]
pub struct Amount {
    currency: Currency,
    value: u32
}
#[derive(Queryable)]
pub struct Delivery {
    order: Order,
    pickup_time: String,
    dropoff_time: String
}

impl Route {
    pub fn new(name: String, stops: Vec<Stop>) -> Route{
        Route{
            name,
            stops
        }
    }
}

impl Stop {
    pub fn new(name: &str) -> Stop {
        Stop{
            uuid: Uuid::new_v4(),
            name: name.to_string()
        }
    }
}

impl Delivery{
    pub fn new(order: Order, pickup_time: String, dropoff_time: String) -> Delivery{
        Delivery{
            order,
            pickup_time,
            dropoff_time
        }
    }
}

impl Order{
    pub fn new(order_type: OrderType, status: OrderStatus, amount: Amount) -> Order {
        Order{
            order_type,
            status,
            amount
        }
    }
}